import groovy.json.JsonSlurper

docker_image_tags_url = "https://registry.hub.docker.com/v2/repositories/juliancillo/webapp/tags/?page_size=20"

@NonCPS
def sortTags(tags) {
    tags.sort { a, b -> b <=> a }
}

try {
    def http_client = new URL(docker_image_tags_url).openConnection() as HttpURLConnection
    http_client.setRequestMethod('GET')
    http_client.connect()

    if (http_client.responseCode == 200) {
        def dockerhub_response = new JsonSlurper().parseText(http_client.inputStream.getText('UTF-8'))
        def image_tag_list = []
        dockerhub_response.results.each { result ->
            image_tag_list.add(result.name.toInteger())    
        }
        image_tag_list = sortTags(image_tag_list)
        println("Image tag list: " + image_tag_list)
        return image_tag_list
    } else {
        println("HTTP response error: " + http_client.responseCode)
        System.exit(0)
    }
} catch (Exception e) {
    e.printStackTrace()
}
